*Challenge Trinomio by Diego Fretes*

DEPENDENCIAS:
-Solo precisa navegador web (chrome recomendable). Está desarrollado con HTML, CSS y JavaScript.

EJECUCIÓN:

-Abrir index.html con el navegador.

-La barra de menú contiene la opción "Administrar personas", donde está la mayoria de la funcionalidad
	-Botón buscar filtra por ID de personas (también por nombre o apellido).
	-Botón ver todas trae todas las personas registradas en la API.
	-Botón nuevo permite un nuevo ingreso.
	-Cada persona buscada tiene 5 funcionalidades:
		>Editar: permite la modificacion de datos personales.
		>Dar de Baja: permite a la persona darse de baja de un curso.
		>Detalles: permite conocer información más precisa acerca de la persona.
		>Anotar: permite asignar un curso nuevo a la persona, de los que están disponibles en la api
		>Eliminar: permite eliminar a la persona.
-Opción Cursos de la barra de menú muestra los cursos disponibles.

Muchas Gracias por tomarse el tiempo de verla!

