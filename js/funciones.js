//Filtra personas por id, nombre o apellido
function BuscarPersonas(){
  var idPer = document.getElementById('idPersona').value
  $(".nueva").slideUp("medium");
  document.getElementById('vistaNueva').innerHTML = " ";
  document.getElementById('vistaPersonas').innerHTML = " ";
  //primer llamado a la API intenta buscar por ID
  $.ajax({
    url:"http://dfrentes.challenge.trinom.io/api/peoples/"+idPer,
    dataType: "json",
    success: function(data){
      vistaPersonas.innerHTML += "<article class='persona'><h3>"+data.first_name+" "+data.last_name+"</h3>"+
      "<h3>"+data.email+"</h3><br>"+
      "<button type='button' onclick=Editar("+data.id+")>Editar</button>"+
      "<button type='button' onclick=DarBaja("+data.id+")>Dar baja</button>"+
      "<button type='button' onclick=Detalles("+data.id+")>Detalles</button>"+
      "<button type='button' onclick=Anotar("+data.id+")>Anotar</button>"+
      "<button type='button' onclick=Eliminar("+data.id+")>Eliminar</button></article>"
    },
    type:'GET',
    error: function() {
      //si no es un id, genera un error. Se pide a la API todas las personas, y se filtra por nombre o apellido
      $.ajax({
        url:"http://dfrentes.challenge.trinom.io/api/peoples",
        dataType: "json",
        success: function(data){
          for(i=0;i<data.total;i++){
            if(data.data[i].first_name==idPer || data.data[i].last_name==idPer){
                vistaPersonas.innerHTML += "<article class='persona'><h3>"+data.data[i].first_name+" "+data.data[i].last_name+"</h3>"+
                "<h3>"+data.data[i].email+"</h3><br>"+
                "<button type='button' onclick=Editar("+data.data[i].id+")>Editar</button>"+
                "<button type='button' onclick=DarBaja("+data.data[i].id+")>Dar baja</button>"+
                "<button type='button' onclick=Detalles("+data.data[i].id+")>Detalles</button>"+
                "<button type='button' onclick=Anotar("+data.data[i].id+")>Anotar</button>"+
                "<button type='button' onclick=Eliminar("+data.data[i].id+")>Eliminar</button></article>"
              }
        }
        },
        type:'GET'
      });

    }

  });

}
//activa la ventana modal, genera el formulario y registra una persona
function Nueva(){
  $(".nueva").slideDown("medium");
  document.getElementById('vistaPersonas').innerHTML = " ";

  document.getElementById('vistaNueva').innerHTML = "<input type='text' class='input-nueva' id='nombre' placeholder='Nombre'><br>"+
    "<input type='text' class='input-nueva' id='apellido' placeholder='Apellido'><br>"+
    "<input type='email' class='input-nueva' id='email' placeholder='Email'><br>"+
    "<p>Seleccione el curso</p>";
    //se pide a la API los cursos disponibles para elegir entre uno de ellos
  $.ajax({
    url:"http://dfrentes.challenge.trinom.io/api/courses",
    dataType: "json",
    success: function(data){
      for(i=0;i<data.length;i++){
        vistaNueva.innerHTML += "<input type='radio' name='curso' value='"+(i+1)+"'><label>"+data[i].name+"</label><br>";
      }
      vistaNueva.innerHTML += "<br><button type='button' id='registro' onclick=Registrar()>Registrar</button></div>";

    },
    type:'GET'
  });
}
//esta funcion es invocada por el boton Registrar, termina el proceso de registro
function Registrar(){
  var nombre = document.getElementById('nombre').value;
  var apellido = document.getElementById('apellido').value;
  var email = document.getElementById('email').value;
  var curso = document.getElementsByName('curso');
  var seleccion = 0;
  var id = 0;
  for(i=0; i<curso.length;i++)
    if(curso[i].checked){
      seleccion = curso[i].value;
    }
    /*$.ajax({
      url:"http://dfrentes.challenge.trinom.io/api/peoples",
      dataType: "json",
      success: function(data){
        id  =data.total + 1;
        console.log(data.total+1);*/
        //busca el curso seleccionado
        $.ajax({
          url:"http://dfrentes.challenge.trinom.io/api/courses?level_id="+seleccion,
          dataType: "json",
          success: function(data){
              hoy = new Date();
               fecha = hoy.getDate() + '-' + ( hoy.getMonth() + 1 ) + '-' + hoy.getFullYear();
               hora = hoy.getHours() + ':' + hoy.getMinutes() + ':' + hoy.getSeconds();
               fechaYHora = fecha + ' ' + hora;
               var json = JSON.stringify({/*id:id,*/first_name:nombre,last_name:apellido,email:email,created_at:fechaYHora,update_at:null,courses:data});
               //metodo post persiste el archivo json generado
               $.ajax({
                       type: "POST",
                       url: "http://dfrentes.challenge.trinom.io/api/peoples",
                       data: json,
                       contentType: 'application/json',
                       success: function(data) {
                        alert("Persona agregada");
                        console.log("Persona añadida!", data);
                        Nueva();
                      },
                       error: function(data) {
                         console.log("No se pudo añadir");
                       }
                     });

          },
          type:'GET'

        });/*
      },
      type:'GET'
    });*/





}
//funcion muestra toas las personas registraadas en la api
function Todas(){
    $(".nueva").slideUp("medium");
    document.getElementById('vistaNueva').innerHTML = " ";
    document.getElementById('vistaPersonas').innerHTML = " ";
    $.ajax({
          url:"http://dfrentes.challenge.trinom.io/api/peoples",
          dataType: "json",
          success: function(data){
              for(i=0;i<data.total;i++){
                    vistaPersonas.innerHTML += "<article class='persona'><h3>"+data.data[i].first_name+" "+data.data[i].last_name+"</h3>"+
                    "<h3>"+data.data[i].email+"</h3><br>"+
                    "<button type='button' onclick=Editar("+data.data[i].id+")>Editar</button>"+
                    "<button type='button' onclick=DarBaja("+data.data[i].id+")>Dar baja</button>"+
                    "<button type='button' onclick=Detalles("+data.data[i].id+")>Detalles</button>"+
                    "<button type='button' onclick=Anotar("+data.data[i].id+")>Anotar</button>"+
                    "<button type='button' onclick=Eliminar("+data.data[i].id+")>Eliminar</button></article>"
               }
          },
          type:'GET'
    });
}
//Elimina una persona segun id
function Eliminar(id){
  var confirmar = confirm("¿Está seguro que desea eliminar?");
  if(confirmar==true){
          $.ajax({
            url: 'http://dfrentes.challenge.trinom.io/api/peoples/'+id,
            type: 'DELETE',
            success: function(result) {
                alert("Eliminada correctamente");
                Todas();
            }
          });
    }
 }
//utiliza la ventana modal para mostrar los detalles de una persona
function Detalles(id){
    $(".ventana").slideDown('slow');
    document.getElementById('contenidoDetalle').innerHTML = "<center><h3>Detalles</h3></center> <hr> ";
    $.ajax({
      url:"http://dfrentes.challenge.trinom.io/api/peoples/"+id,
      dataType: "json",
      success: function(data){
        contenidoDetalle.innerHTML += "<p><b>ID: </b>"+data.id+"</b><p><b>Nombre: </b>"+data.first_name+" "+data.last_name+"</p>"+
        "<p><b>Correo Electrónico: </b>"+data.email+"</p>"+
        "<p><b>Fecha en la que se inscribió: </b>"+data.created_at+"<p>"+
        "<br><p><b>Cursos que se inscribió</b></p><hr>";
        for(i=0;i<data.courses.length;i++){
            contenidoDetalle.innerHTML += "<p><b>Curso: </b>"+data.courses[i].name+"</p>"+
            "<p><b>Lenguaje: </b>"+data.courses[i].language.name+"</p>"+
            "<p><b>Nivel: </b>"+data.courses[i].level.name+"</p> <hr>";
        }
      },
      type:'GET'

    });

}

function Cerrar(){
  $(".ventana").slideUp('fast');
}


function Editar(id){
  $(".ventana").slideDown('fast');
  document.getElementById('contenidoDetalle').innerHTML = "<center><h3>Editar</h3></center> <hr> ";

  $.ajax({
    url:"http://dfrentes.challenge.trinom.io/api/peoples/"+id,
    dataType: "json",
    success: function(data){
      contenidoDetalle.innerHTML += "<label class='l-edit'><b>Nombre</b></label><input id='n-edit' value='"+data.first_name+"'>"+
      "<br><label class='l-edit'><b>Apellido</b></label><input id='a-edit' value='"+data.last_name+"'>"+
      "<br><label class='l-edit'><b>Correo Electrónico</b></label><input id='e-edit' value='"+data.email+"'>"+
      "<br><button type='button' id='btn-guardar' onclick=Guardar("+data.id+")>Guardar</button>";

    },
    type:'GET'

  });

}

function Guardar(id){

      var fecha = new Date();
      var nombre = document.getElementById("n-edit").value;
      var apellido = document.getElementById("a-edit").value;
      var email = document.getElementById("e-edit").value;

      $.ajax({
        url:"http://dfrentes.challenge.trinom.io/api/peoples/"+id,
        dataType: "json",
        success: function(data){
            var json = JSON.stringify({first_name:nombre,last_name:apellido,email:email,created_at:data.created_at,update_at:fecha,courses:data.courses});
            $.ajax({
                  url: "http://dfrentes.challenge.trinom.io/api/peoples/"+id,
                  type: 'PUT',
                  contentType: 'application/json',
                  data: json,
                  success: function(data) {
                      alert('Datos guardados');
                      Cerrar();
                      Todas();
                  }
            });

        },
        type:'GET'

      });
  }

// usa la ventana modal para seleccionar un curso a dar de baja. implementa eliminarCurso()
function DarBaja(id){
  $(".ventana").slideDown('fast');
  document.getElementById('contenidoDetalle').innerHTML = "<center><h3>Dar de baja curso</h3></center> <hr> ";

  $.ajax({
    url:"http://dfrentes.challenge.trinom.io/api/peoples/"+id,
    dataType: "json",
    success: function(data){
      var cursos = data.courses;
      console.log(cursos);
      for(i=0;i<cursos.length;i++){
      contenidoDetalle.innerHTML += "<label>"+cursos[i].name+"</label><button type='button' onclick=eliminarCurso("+cursos[i].id+","+id+")>Eliminar</button><br><hr>";
      }


    },
    type:'GET'

  });

}

//utiliza la ventana modal para seleccionar un curso de los disponibles en la API, para dar de alta. Implementa agregarCurso
function Anotar(id){
  $(".ventana").slideDown('fast');
  document.getElementById('contenidoDetalle').innerHTML = "<center><h3>Anotar a un curso</h3></center> <hr> ";

  $.ajax({
    url:"http://dfrentes.challenge.trinom.io/api/courses",
    dataType: "json",
    success: function(data){

      for(i=0;i<data.length;i++){
        contenidoDetalle.innerHTML += "<label>"+data[i].name+"</label><button type='buttton' onclick=agregarCurso("+data[i].id+","+id+")>Anotar</button><br><hr>";
      }


    },
    type:'GET'

  });

}

//recibe id de curso y id de persona. Agrega el curso al array de cursos de la persona
 function agregarCurso(id,persona){
     $.ajax({
       url:"http://dfrentes.challenge.trinom.io/api/courses?level_id="+id,
       dataType: "json",
       success: function(data){
             var cursoNuevo = data[0];
             $.ajax({
               url:"http://dfrentes.challenge.trinom.io/api/peoples/"+persona,
               dataType: "json",
               success: function(data){
                  var fecha = new Date();
                  var cursos = data.courses;
                  cursos.push(cursoNuevo);
                  var json = JSON.stringify({first_name:data.first_name,last_name:data.last_name,email:data.email,created_at:data.created_at,update_at:fecha,courses:cursos});
                    $.ajax({
                        url: "http://dfrentes.challenge.trinom.io/api/peoples/"+persona,
                        type: 'PUT',
                        contentType: 'application/json',
                        data: json,
                        success: function(data) {
                          alert('Datos guardados');
                          Cerrar();
                        }
                      });
                },
               type:'GET'
               });
          },
      type:'GET'
     });
 }

//recibe id de curso y id de persona. Elimina el curso del array de cursos de la persona
function eliminarCurso(id,persona){
  var confirmar = confirm("¿Está seguro que desea dar de baja del curso?");
  if(confirmar==true){
          $.ajax({
                url:"http://dfrentes.challenge.trinom.io/api/peoples/"+persona,
                dataType: "json",
                success: function(data){
                      var cursos = data.courses;
                      var nuevoc = [];
                      for(i=0;i<cursos.length;i++){
                          if(cursos[i].id!=id){
                            nuevoc.push(cursos[i]);
                          }
                      }
                      var fecha = new Date();
                      var json = JSON.stringify({first_name:data.first_name,last_name:data.last_name,email:data.email,created_at:data.created_at,update_at:fecha,courses:nuevoc});
                      $.ajax({
                            url: "http://dfrentes.challenge.trinom.io/api/peoples/"+persona,
                            type: 'PUT',
                            contentType: 'application/json',
                            data: json,
                            success: function(data) {
                              alert('Dado de baja correctamente');
                              Cerrar();
                            }

                          });
                      },
                  type:'GET'

          });

}
}



var buscar = document.getElementById('buscarPersona');
buscar.addEventListener('click',BuscarPersonas);

var nueva = document.getElementById('nueva');
nueva.addEventListener('click',Nueva);

var mostrar = document.getElementById('ver');
mostrar.addEventListener('click',Todas);

var cerrar = document.getElementById('cerrar');
cerrar.addEventListener('click',Cerrar);
